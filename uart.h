#ifndef UART_H
#define UART_H

#define UART_BAUD_SELECT(baudRate,xtalCpu) ((xtalCpu)/((baudRate)*16l)-1)
#define UART_BAUD_SELECT_DOUBLE_SPEED(baudRate,xtalCpu) (((xtalCpu)/((baudRate)*8l)-1)|0x8000)

#ifndef UART_RX_BUFFER_SIZE
#define UART_RX_BUFFER_SIZE 4
#endif

#ifndef UART_TX_BUFFER_SIZE
#define UART_TX_BUFFER_SIZE 16
#endif

#if ( (UART_RX_BUFFER_SIZE+UART_TX_BUFFER_SIZE) >= (RAMEND-0x60 ) )
#error "size of UART_RX_BUFFER_SIZE + UART_TX_BUFFER_SIZE larger than size of SRAM"
#endif

#define UART_FRAME_ERROR      0x0800              /* Framing Error by UART       */
#define UART_OVERRUN_ERROR    0x0400              /* Overrun condition by UART   */
#define UART_BUFFER_OVERFLOW  0x0200              /* receive ringbuffer overflow */
#define UART_NO_DATA          0x0100              /* no receive data available   */

#define uart_puts_P(__s)       uart_puts_p(PSTR(__s))


extern void uart_init(unsigned int baudrate);
extern unsigned int uart_getc(void);
extern void uart_putc(unsigned char data);
extern void uart_puts(const char *s );
extern void uart_puts_p(const char *s );
extern int uart_available(void);
extern void uart_flush(void);

#endif

